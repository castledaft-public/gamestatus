# GameStatus

Game Status ingests metrics from your game server and provides a status page for your players to use.

## Planned features

- Status page
    - Is it up
    - Current ping latency
- Activity list
    - Server downtime
    - Latency spikes
- Widgets
    - Online players
    - Metric graphs unique per game
        - Minecraft
            - Deaths
            - Items mined (ability to select what from a dropdown)
            - Entity Kills
        - Valheim
            - Deaths
    - Most active players
    - Player list
    - Server creation date
    - Connection information
- Games
    - Minecraft
    - Valheim
    - Satisfactory
    - 7 days to die

## Authors

Jack 'SurDaft' Stupple
jack.stupple@protonmail.com
https://gitlab.com/surdaft - https://jackstupple.uk

Adam 'Faybuh' Farmer
N/A
https://gitlab.com/faybuh

## Contributing

If you would like to contribute to the project then please take a look at the roadmap and then implement it, don't worry about how you do it or how good you feel at development, we are all here to learn and the PR process is in place to allow us to talk about the code before it goes to `main`. :)
